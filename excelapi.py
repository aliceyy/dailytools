"""
xlsx to csv converter

"""
from os import path
from utils import get_filename_without_ext


def xls2csv(source_file, dest_path):
    """
    Convert the source file of format .xlsx or .xls to .csv. The formula is also evaluated.

    Args:
        source_file: Absolute path of excel file.
        dest_path: destination path, the result .csv file will be put in this path with the same name \
            of given excel file.

    """
    import subprocess
    if not path.exists(source_file):
        raise IOError("The source file '{}' does not exist.".format(source_file))
    vbs_file = path.join(path.dirname(__file__), "xls2csv.vbs")
    filename_csv = get_filename_without_ext(source_file) + '.csv'
    subprocess.call(['cscript.exe', vbs_file, source_file, path.join(dest_path, filename_csv)])
