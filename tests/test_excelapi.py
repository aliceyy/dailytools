"""
Test the module xls2csv
"""

from openpyxl import Workbook
import datetime
from os import path, remove

# from current project
import excelapi


def test_xls2csv():
    wb = Workbook()
    ws = wb.active

    # Data can be assigned directly to cells
    ws['A1'] = 42
    # Rows can also be appended
    ws.append([1, 2, 3])

    # Python types will automatically be converted
    ws['A2'] = datetime.datetime.now()
    ws['A3'] = '=SUM(A1 + 2)'

    # Save the file
    filename = "sample.xlsx"
    wb.save(filename)
    local_path = path.abspath(".")
    source_file = path.join(local_path, filename)
    excelapi.xls2csv(source_file, local_path)
    expected_csv_file_loc = path.join(local_path, "sample.csv")
    assert path.exists(expected_csv_file_loc)
    remove(source_file)
    remove(expected_csv_file_loc)

