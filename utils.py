"""
Utilities
"""
from os import path


def get_filename_without_ext(filename):
    result = path.basename(filename)
    result = path.splitext(result)[0]
    return result
